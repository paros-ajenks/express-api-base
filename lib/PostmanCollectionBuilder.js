const ExpressParser = require("./ExpressParser");
const moment = require("moment");
const uuid = require("uuid/v4");
const path = require("path");
const fs = require("fs");
const qs = require("qs");

const parsePackageJson = () => {
    let packageJson = require(path.join(process.cwd(), "package.json"));
    if(!packageJson._postman_id) {
        packageJson._postman_id = uuid();
        fs.writeFileSync(path.join(process.cwd(), "package.json"), JSON.stringify(packageJson, null, 4));
    }

    return {
        postmanId: packageJson._postman_id,
        title: packageJson.title || "Demo API",
        version: packageJson.version,
        description: packageJson.description || "Auto-generated documentation"
    };
};

const getDemoForType = desc => {
    switch(desc.type) {
        case "array": return [getDemoForType(desc.items[0])];
        case "object": return Object.keys(desc.keys).reduce((obj, key) => ({
            ...obj,
            [key]: getDemoForType(desc.keys[key])
        }), {});
        case "string": return "String";
        case "number": return desc.allow ? desc.allow[0] : 1234;
        case "boolean": return false;
        case "date": return moment().toString("YYYY-MM-DD");

        default: return null;
    }
}

const processBody = body => Object.keys(body)
.filter(x => body[x].flags ? body[x].flags.presence !== "optional" : true)
.reduce((vals, key) => ({
    ...vals,
    [key]: getDemoForType(body[key].describe())
}), {});

const parseRoute = (route, parent, variables) => {
    for(let method in route.methods) {
        let [{ headers = {}, path = {}, query = {}, body = {} }] = route.methods[method];

        let reqObj = {
            name: route.path,
            protocolProfileBehavior: {},
            request: {
                method: method.toUpperCase(),
                header: [],
                body: {},
                url: {
                    raw: route.path.replace(/:([^:/]+)/gmi, (_,x) => `{{${x}}}`),
                    host: [],
                    path: [],
                    query: []
                }
            }
        }

        Object.keys(headers).forEach(header => {
            let example = getDemoForType(headers[header].describe());
            reqObj.request.header.push({
                key: header,
                value: example,
                type: "text"
            });
        });

        Object.keys(path).forEach(item => {
            let desc = path[item].describe();
            let example = getDemoForType(desc);
            variables.push({
                id: uuid(),
                type: desc.type,
                name: item,
                value: example
            });
        });

        Object.keys(query).forEach(param => {
            let desc = query[param].describe();
            let example = getDemoForType(desc);
            reqObj.request.url.query.push({
                key: param,
                value: example
            });
        });

        if(reqObj.request.url.query.length) {
            reqObj.request.url.raw += `?${qs.stringify(reqObj.request.url.query.reduce((a,k) => ({...a,[k.key]:k.value}), {}))}`
        }

        if(Object.keys(body).length) {
            reqObj.request.body = {
                mode: "raw",
                raw: JSON.stringify(processBody(body), null, 4),
                options: {
                    raw: {
                        language: "json"
                    }
                }
            }
        }
        
        parent.item.push(reqObj);
    }

    return parent;
};

module.exports = ({
    app,
    dest = "postman_collection.json"
}) => {
    let { postmanId, title, version, description } = parsePackageJson();
    let routes = ExpressParser(app);

    let spec = {
        info: {
            "_postman_id": postmanId,
            name: title,
            schema: "https://schema.getpostman.com/json/collection/v2.1.0/collection.json"
        },
        item: []
    };

    let variables = [];

    for(let route of routes) {
        let methods = Object.keys(route.methods);

        if(methods.length > 1) {
            let tree = parseRoute(route, {
                name: route.path,
                item: [],
                protocolProfileBehavior: {}
            }, variables);

            spec.item.push(tree);
        } else {
            parseRoute(route, spec, variables);
        }
    }

    spec.variable = variables;

    if(dest === "memory") return spec;

    let destFile = path.join(process.cwd(), dest);
    fs.writeFileSync(destFile, JSON.stringify(spec, null, 4));
};