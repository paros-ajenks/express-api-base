const merge = require("lodash/merge");
const sortBy = require("lodash/sortBy");

const regexpExpressRegexp = /^\/\^\\\/(?:(:?[\w\\.-]*(?:\\\/:?[\w\\.-]*)*)|(\(\?:\(\[\^\\\/]\+\?\)\)|\(\?:\(\[0-9]\+\)\)))\\\/.*/;
const regexpExpressParam = /(\(\?:\(\[\^\\\/]\+\?\)\)|\(\?:\(\[0-9]\+\)\))/g;

const getRouteMethods = route => Object.keys(route.methods).filter(x => x !== "_all").map(x => x.toLowerCase());

const hasParams = pathRegexp => regexpExpressParam.test(pathRegexp);

const getRouteSchemas = route => {
    let schemas = {
        headers: {},
        path: {},
        query: {},
        body: {},
        metadata: {}
    };

    route.stack.forEach(layer => {
        if(layer.handle) {
            if(layer.handle.schemas) {
                merge(schemas, layer.handle.schemas);
            }

            if(layer.handle.metadata) {
                schemas.metadata = {
                    ...schemas.metadata,
                    ...layer.handle.metadata
                };
            }
        }
    });

    return schemas;
};

const parseExpressRoute = (route, basePath) => {
    return {
        path: basePath + (basePath && route.path === "/" ? "" : route.path),
        methods: getRouteMethods(route),
        schemas: getRouteSchemas(route)
    };
};

const parseExpressPath = (expressPathRegexp, params) => {
    let parsedPath = regexpExpressRegexp.exec(expressPathRegexp);
    let parsedRegexp = expressPathRegexp;
    let paramIdx = 0;

    while(hasParams(parsedRegexp)) {
        let paramId = `:${params[paramIdx].name}`;
        parsedRegexp = parsedRegexp.toString().replace(/\(\?:\(\[\^\\\/]\+\?\)\)/, paramId);
        paramIdx++;
    }

    if(parsedRegexp !== expressPathRegexp) {
        parsedPath = regexpExpressRegexp.exec(parsedRegexp);
    }

    parsedPath = parsedPath[1].replace(/\\\//g, "/");
    return parsedPath;
};

/** @returns {ExpressParser.DescribedRoute[]} */
const getEndpoints = app => parseEndpoints(app);

const addEndpoint = (endpoints, newEndpoint) => {
    let foundEndpointIdx = endpoints.findIndex(item => item.path === newEndpoint.path);
    if(foundEndpointIdx > -1) {
        newEndpoint.methods.forEach(method => {
            if(!endpoints[foundEndpointIdx].methods[method])
                endpoints[foundEndpointIdx].methods[method] = [];

            endpoints[foundEndpointIdx].methods[method].push(newEndpoint.schemas);
        });
    } else {
        endpoints.push({
            path: newEndpoint.path,
            methods: newEndpoint.methods.reduce((api, method) => ({
                ...api,
                [method]: [newEndpoint.schemas]
            }), {})
        });
    }
    
    return endpoints;
};

/** @returns {ExpressParser.DescribedRoute[]} */
const parseEndpoints = (app, basePath = "", endpoints = []) => {
    let stack = app.stack || (app._router && app._router.stack) || [];

    for(let stackItem of stack) {
        if(stackItem.route) {
            let endpoint = parseExpressRoute(stackItem.route, basePath);
            endpoints = addEndpoint(endpoints, endpoint);
        } else if(stackItem.name === "router" || stackItem.name === "bound dispatch") {
            if(regexpExpressRegexp.test(stackItem.regexp) && !stackItem.handle.ignore) {
                let parsedPath = parseExpressPath(stackItem.regexp, stackItem.keys);
                parseEndpoints(stackItem.handle, `${basePath}/${parsedPath}`, endpoints);
            } else if(!stackItem.handle.ignore) {
                parseEndpoints(stackItem.handle, basePath, endpoints);
            }
        }
    }

    return sortBy(endpoints, "path");
};

exports = module.exports = getEndpoints;