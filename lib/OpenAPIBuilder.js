const ExpressParser = require("./ExpressParser");
const j2s = require("joi-to-swagger");
const merge = require("lodash/merge");
const joi = require("joi");
const path = require("path");
const YAML = require("yaml");
const fs = require("fs");

const parsePackageJson = () => {
    let packageJson = require(path.join(process.cwd(), "package.json"));
    return {
        version: packageJson.version,
        title: packageJson.title || "Demo API",
        description: packageJson.description || "Auto-generated documentation"
    };
};

const STANDARD_RESPONSES = {
    200: { description: "OK" },
    400: { description: "Validation error" },
    404: { description: "Not found" },
    500: { description: "Server error" }
};

/**
 * TODO: Better joi type inference (e.g. enums)
 * TODO: Support more middleware metadata
 * TODO: Optimize
 */

module.exports = ({
    app,
    dest = "openapi.yaml"
}) => {
    let { title, version, description } = parsePackageJson();
    let destFile = path.join(process.cwd(), dest);
    let routes = ExpressParser(app);

    let spec = {
        openapi: "3.0.0",
        info: {
            title,
            description,
            version
        },
        paths: {}
    };

    for(let route of routes) {
        let methods = {};

        for(let method in route.methods) {
            let parameters = [];
            let reqBody = {};
            let impl = route.methods[method];

            let m = {};
            for(let { headers = {}, path = {}, query = {}, body = {}, metadata = {} } of impl) {
                if(Object.keys(headers).length) {
                    Object.keys(headers).forEach(param => {
                        let { description, ...schema } = j2s(headers[param]).swagger;
                        if(!description || !description.length) description = "";
                        parameters.push({ in: "header", name: param, schema, description });
                    });
                }

                if(Object.keys(path).length) {
                    Object.keys(path).forEach(param => {
                        let { description, ...schema } = j2s(path[param]).swagger;
                        if(!description || !description.length) description = "";
                        parameters.push({ in: "path", name: param, schema, description, required: true });
                    });
                }

                if(Object.keys(query).length) {
                    Object.keys(query).forEach(item => {
                        let { description, ...schema } = j2s(query[item]).swagger;
                        if(!description || !description.length) description = "";
                        parameters.push({ in: "query", name: item, schema, description });
                    });
                }

                if(Object.keys(body).length) {
                    let parsed = j2s(joi.object(body)).swagger;
                    merge(reqBody, {
                        content: {
                            "application/json": {
                                schema: parsed
                            }
                        }
                    });
                }

                m.description = metadata.description;
                m.deprecated = metadata.deprecated;
                m.tags = metadata.tags;
            }

            if(parameters.length) m.parameters = parameters;
            if(reqBody.content) m.requestBody = reqBody;
            m.responses = STANDARD_RESPONSES;

            methods[method] = m;
        }

        spec.paths[route.path] = methods;
    }

    if(dest === "memory") return spec;

    fs.writeFileSync(destFile, YAML.stringify(spec));
};