const express = require("express");
const bodyParser = require("body-parser");
const jsonpayload = require("./middleware/jsonpayload");
const swagger = require("./middleware/swagger");
const cors = require("./middleware/origins");
const errors = require("./middleware/errors");
const healthService = require("./middleware/health");
const subdomainMiddleware = require("./middleware/subdomain");
const helmet = require("helmet");

module.exports = ({
    config,
    routes,

    middleware,
    addMiddleware,

    maxPayloadSize = "10mb",
    helmetConfig,

    disableHealthEndpoint,
    disableHelmet,
    disableSwagger,
    disableBodyParser,
    disableCorsModule,
    disableErrorHandling,
    disableJsonPayload,

    enableSubdomainMiddleware,

    parseRaw = true,
    parseText = true,
    parseJson = true,
    parseUrlEncoded = true
}) => {
    const app = express();

    if(!disableHealthEndpoint) app.use(healthService);
    if(!disableJsonPayload)    app.use(jsonpayload);
    if(!disableCorsModule)     app.use(cors(config));
    if(!disableHelmet)         app.use(helmet(helmetConfig));
    if(enableSubdomainMiddleware) app.use(subdomainMiddleware);

    if(!disableBodyParser) {
        if(parseRaw)        app.use(bodyParser.raw({ limit: maxPayloadSize }));
        if(parseText)       app.use(bodyParser.text({ limit: maxPayloadSize }));
        if(parseJson)       app.use(bodyParser.json({ limit: maxPayloadSize }));
        if(parseUrlEncoded) app.use(bodyParser.urlencoded({ extended: true, limit: maxPayloadSize }));
    }

    // Right here is where you'd use app-specific middleware
    if(Array.isArray(middleware) && middleware.length) app.use(...middleware);
    if(typeof addMiddleware === "function") addMiddleware(app);

    if(routes) app.use(routes);
    if(!disableSwagger) app.use(swagger(app));
    if(!disableErrorHandling) app.use(errors);

    return app;
};