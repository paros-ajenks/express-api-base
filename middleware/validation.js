const joi = require("joi");
const JoiParser = require("../lib/JoiParser");

const handleError = (error, res) => {
    if(error.name == "ValidationError") {
        let { context, message } = error.details[0];

        return res.sendError("validation", message, 400, { field: context.key });
    } else {
        return res.sendError("generic", error, 500);
    }
};

const individualHandler = ({ prop, dest, obj, schema, options }) => {
    if(!dest) dest = prop;

    let middleware = (req, res, next) => {
        try {
            if(req.query["x-validation-schema"]) {
                return res.status(200).send(JoiParser.extractProperties({ [prop]: schema }));
            }

            if(req.query["x-joi-schema"]) {
                return res.status(200).send(JoiParser.serializeSchema({ [prop]: schema }));
            }

            if(!req.validation) req.validation = {};

            let { error, value } = schema.validate(req[prop], { allowUnknown: true, ...options });
            if(error) return handleError(error, res);

            req.validation[dest] = value;

            return next();
        } catch(ex) {
            return next(ex);
        }
    };

    middleware.schemas = { ...middleware.schemas, [prop]: obj };
    Object.defineProperty(middleware, "name", { value: "joi validation", writable: true });

    return middleware;
};

exports = module.exports = ({
    headers, path, query, body
}, options) => {
    let schema = {};
    if(headers) schema.headers = joi.object(headers).unknown(true);
    if(path) schema.params = joi.object(path).required();
    if(query) schema.query = joi.object(query);
    if(body) schema.body = joi.object(body);

    let _schema = joi.object(schema).unknown(true);

    let handler = (req, res, next) => {
        try {
            if(req.query["x-validation-schema"]) {
                return res.status(200).send(JoiParser.extractProperties(schema));
            }

            if(req.query["x-joi-schema"]) {
                return res.status(200).send(JoiParser.serializeSchema(schema));
            }

            if(!req.validation) req.validation = {};

            let { error, value } = _schema.validate({ headers: req.headers, path: req.path, query: req.query, body: req.body }, { ...options });
            if(error) return handleError(error, res);

            Object.assign(req.validation, value);
            return next();
        } catch(ex) {
            return next(ex);
        }
    };

    handler.schemas = { ...handler.schemas, headers, path, query, body };
    Object.defineProperty(handler, "name", { value: "joi validation", writable: true });

    return handler;
};

exports.validateHeaders = (obj, options = {}) => {
    let schema = joi.object(obj);
    return individualHandler({ prop: "headers", obj, schema, options });
};

exports.validatePath = (obj, options = {}) => {
    let schema = joi.object(obj);
    return individualHandler({ prop: "params", dest: "path", obj, schema, options });
};

exports.validateQuery = (obj, options = {}) => {
    let schema = joi.object(obj);
    return individualHandler({ prop: "query", obj, schema, options });
};

exports.validateBody = (obj, options = {}) => {
    let schema = obj.$_root ? obj : joi.object(obj);
    return individualHandler({ prop: "body", obj, schema, options });
};
