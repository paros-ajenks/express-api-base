const router = require("express").Router();
const withMetadata = require("./metadata");
module.exports = router;

const middleware = [
    withMetadata({ description: "Endpoint used for ECS to check instance health" })
];

router.get("/health", ...middleware, (req, res) => res.status(200).send("Health OK"));
