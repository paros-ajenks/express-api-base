const minimatch = require("minimatch");
const cors = require("cors");

let originParser = config => config.cors.origins.includes("*") ? "*" : (origin, cb) => {
    let foundMatch = config.cors.origins.find(x => minimatch(origin, x));

    if(foundMatch) {
        return cb(null, true);
    } else {
        let error = new Error("Not allowed by CORS");
        error._statusCode = 401;
        return cb(error);
    }
};

module.exports = appconfig => cors({
    origin: originParser(appconfig)
});
