const UserSafeError = require("../utils/UserSafeError");

class JsonPayload {
    payload = null;
    error = null;
    status = 200;
    metadata = {};

    setPayload = (payload, status = 200, metadata = {}) => {
        this.status = status;
        this.payload = payload;
        this.error = null;
        this.details = null;
        this.metadata = metadata;
    }

    setError = (type, details, status = 500, metadata = {}) => {
        this.error = type;
        this.details = details;
        this.payload = null;
        this.status = status;
        this.metadata = metadata;
    }

    setStatus = status => this.status = status;

    noContent = () => {
        this.payload = null;
        this.status = 204;
        this.error = null;
        this.details = null;
    }

    send = res => {
        res.status(this.status);

        if(this.status === 204) {
            res.send(null);
        } else if(this.error) {
            res.send({
                error: this.error,
                details: this.details,
                payload: null,
                ...this.metadata
            });
        } else {
            res.send({
                error: null,
                payload: this.payload,
                ...this.metadata
            });
        }
    }
}

module.exports = (req, res, next) => {
    let payload = new JsonPayload();

    res.payload = payload;
    res.setPayload = payload.setPayload;
    res.setError = payload.setError;
    res.noContent = payload.noContent;

    res.sendNoContent = () => {
        res.noContent();
        return res.payload.send(res);
    };

    res.sendPayload = (...args) => {
        if(args.length) {
            res.payload.setPayload(...args);
            return res.payload.send(res);
        } else return res.payload.send(res);
    };

    res.sendError = (error, details, status, metadata) => {
        res.setError(error, details, status, metadata);
        return res.payload.send(res);
    };

    res.handleError = (ex, type, status) => {
        if(ex instanceof UserSafeError) {
            if(ex.log) {
                console.error(ex);
            }

            res.sendError(type || ex.type, ex.message, status || ex._statusCode || (ex.type === "auth" ? 403 : 500));
        } else if(typeof ex === "string") {
            console.error(ex);
            res.sendError(type || "generic", ex);
        } else {
            console.error(ex);
            res.sendError(type || "generic", "An unknown error has occurred");
        }
    }

    return next();
};
