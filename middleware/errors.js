module.exports = (error, req, res, next) => {
    return res.handleError(error, "generic", error._statusCode);
};
