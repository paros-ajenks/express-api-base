module.exports = ({
    description,
    secure = false,
    deprecated = false,
    responses = {},
    tags
} = {}) => {
    let handler = (req, res, next) => next();
    handler.metadata = {
        ...handler.metadata, description, responses, secure, deprecated, tags
    };

    return handler;
};
