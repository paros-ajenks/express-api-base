declare namespace ExpressParser {
    interface DescribedRoute {
        path: string;
        methods: RouteVerbSchema;
    }

    interface RouteVerbSchema {
        [key: string]: RouteSchema;
    }

    interface RouteSchema {
        headers: SchemaValue;
        path: SchemaValue;
        query: SchemaValue;
        body: SchemaValue;
    }

    interface SchemaValue {
        [key: string]: Object;
    }
}