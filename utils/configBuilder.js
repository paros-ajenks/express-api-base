/**
 * @template T
 * @param {T} DEFAULT_CONFIG
 * @returns {T}
 */
const RC = DEFAULT_CONFIG => {
    let RC = require("rc");
    return RC("app", DEFAULT_CONFIG);
};

/**
 * @template T
 * @param {T} DEFAULT_CONFIG
 * @returns {T}
 */
const YAML = DEFAULT_CONFIG => {
    let merge = require("lodash/merge");
    let path = require("path");
    let fs = require("fs");
    let YAML = require("yaml");


    let configDir = path.join(process.cwd(), "config");
    let defaultConfig = path.join(configDir, "default.yaml");
    let envConfig = path.join(configDir, `${process.env.NODE_ENV}.yaml`);

    let config = DEFAULT_CONFIG;

    if(fs.existsSync(defaultConfig)) {
        let src = fs.readFileSync(defaultConfig).toString("utf8");
        let cfg = YAML.parse(src);
        merge(config, cfg);
    }

    if(fs.existsSync(envConfig)) {
        let src = fs.readFileSync(envConfig).toString("utf8");
        let cfg = YAML.parse(src);
        merge(config, cfg);
    }

    return config;
};

exports = module.exports = YAML;
exports.yaml = YAML;
exports.rc = RC;
